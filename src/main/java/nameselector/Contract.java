package nameselector;

import nameselector.model.Item;

import javax.swing.*;

public class Contract {
    public interface View {
        void updateList(DefaultListModel<Item> listModel);
    }

    public interface Presenter {
        void prepareList();
        void onInput(String inputText);
        Item getItem(int index);
    }
}
