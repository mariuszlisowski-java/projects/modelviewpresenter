package nameselector.presenter;

import nameselector.Contract;
import nameselector.model.Item;
import nameselector.model.Person;
import nameselector.model.PersonProvider;

import javax.swing.*;
import java.util.AbstractList;
import java.util.List;
import java.util.stream.Collectors;

// presenter
public class PersonPresenter implements Contract.Presenter {
    private final Contract.View view;
    private final PersonProvider provider;

    // initializing view & provider references
    public PersonPresenter(Contract.View view, PersonProvider provider) {
        this.view = view;
        this.provider = provider;
    }

    @Override
    public void prepareList() {
        view.updateList(provider.getItems());
    }

    @Override
    public void onInput(String inputText) {
        inputText = inputText.trim();
        if (!inputText.equals("")) {
            String input = String.valueOf(inputText.charAt(0)).toUpperCase() + inputText.substring(1);
            List<Item> asSurname = asList(provider.getItems())
                    .stream()
                    .filter(item -> ((Person) item).getName().contains(input))
                    .collect(Collectors.toList());

            DefaultListModel<Item> output = new DefaultListModel<>();
            for (Item item : asSurname) {
                output.addElement(item);
            }

            view.updateList(output);
        } else {
            view.updateList(provider.getItems());
        }
    }

    @Override
    public Item getItem(int index) {
        return provider.getItems().get(index);
    }

    private List<Item> asList(DefaultListModel<Item> listModel) {
        return new AbstractList<Item>() {
            @Override
            public Item get(int index) {
                return listModel.elementAt(index);
            }

            @Override
            public int size() {
                return listModel.getSize();
            }
        };
    }

}
