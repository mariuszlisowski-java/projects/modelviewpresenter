package nameselector.view;

import nameselector.Contract;
import nameselector.model.Item;
import nameselector.model.PersonProvider;
import nameselector.presenter.PersonPresenter;

import javax.swing.*;
import java.awt.event.KeyEvent;

// view
public class WindowView implements Contract.View {
    // initializing presenter (with view & provider)
    private final Contract.Presenter presenter = new PersonPresenter(this, new PersonProvider());

    private final JFrame frame;
    private final JTextField inputField;
    private final JList<Item> results;
    private final JScrollPane scrollPane;

    public WindowView() {
        // set frame
        frame = new JFrame("Name selector");
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(true); // TO BE CHANGED

        // set prompt label
        JLabel promptLabel = new JLabel("Filter by name:");
        promptLabel.setBounds(20, 20, 100, 30);

        // set input field
        inputField = new JTextField();
        inputField.setBounds(120, 20, 200, 30);
        inputField.addKeyListener(new KeyReleasedListener() {
            @Override
            public void keyReleased(KeyEvent e) {
                presenter.onInput(inputField.getText());
            }
        });

        // set results list wrapped with scroll pane
        results = new JList<>();
        results.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        scrollPane = new JScrollPane(results);
        scrollPane.setBounds(120, 60, 200, 250);

        // fill frame
        frame.add(promptLabel);
        frame.add(inputField);
        frame.add(scrollPane);
        frame.setVisible(true);

        // populate the list with initial results
        presenter.prepareList();
    }

    @Override
    public void updateList(DefaultListModel<Item> listModel) {
        results.setModel(listModel);
    }

}
