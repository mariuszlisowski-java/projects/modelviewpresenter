package nameselector.model;

import javax.swing.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class PersonProvider implements ItemProvider {
    private DefaultListModel<Item> persons;

    @Override
    public DefaultListModel<Item> getItems() {
        return persons;
    }

    public PersonProvider() {
        this.persons = new DefaultListModel<>();
        loadPersons();
    }

    private void loadPersons() {
        try {
            List<String> names = Files.readAllLines(Paths.get("persons.text"));
            for (String name : names) {
                String[] nameAndSurname = name.split(" ");
                if (nameAndSurname.length == 2) {
                    String firstName = nameAndSurname[0];
                    String secondName = nameAndSurname[1];
                    Person person = new Person(firstName, secondName);
                    persons.addElement(person);
                }
            }
        } catch (IOException e) {
            // TODO
        }
    }
}
