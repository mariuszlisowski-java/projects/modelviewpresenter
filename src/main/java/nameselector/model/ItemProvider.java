package nameselector.model;

import javax.swing.*;

public interface ItemProvider {
     DefaultListModel<? extends Item> getItems();
}
