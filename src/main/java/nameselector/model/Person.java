package nameselector.model;

import java.util.UUID;

public class Person extends Item {
    private String name;
    private String surname;

    public Person(String name, String surname) {
        this.surname = surname;
        this.name = name;
        this.uuid = UUID.randomUUID();
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        return name + ' ' + surname;
    }
}
